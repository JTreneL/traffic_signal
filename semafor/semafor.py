#Semafor zelená,žlutá,červená - Cykly 5,10,5s
import time  
import sys


semafor = """SIGNAL       
            -OOO-
            -OOO-"""

#Colours of singnal 
def colored(r, g, b, text):
    return "\033[38;2;{};{};{}m{} \033[38;2;255;255;255m".format(r, g, b, text)
zelenýsemafor = colored(0,255,0, semafor)
žlutýsemafor = colored(200,250,0, semafor)
červenýsemafor = colored(255,0,0, semafor)



def main():
    while True:
        print (zelenýsemafor)
        time.sleep(5)
        print (žlutýsemafor)
        time.sleep(10)
        print (červenýsemafor)
        time.sleep(5)

if __name__ == "__main__":
    sys.exit(main())