from setuptools import setup, find_packages

setup(
    name='semafor',  # Required
    version='2.0.0',  # Required
    url='https://gitlab.com/JTreneL/traffic_signal',  # Optional
    author='JTrenel',  # Optional
    author_email='Janlenert@email.cz',  # Optional
    packages=find_packages(),  # Required
    entry_points={"console_scripts": ["semafor = semafor.semafor:main"]},
    #semafor = semafor.Semafor:main
    #jméno package = jméno_package.jméno_scriptu:metoda_která_se_má_spustit_na_začátku
    
)
